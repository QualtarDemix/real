/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 11, 2019 1:12:50 AM                     ---
 * ----------------------------------------------------------------
 */
package com.real.realoccaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedRealoccaddonConstants
{
	public static final String EXTENSIONNAME = "realoccaddon";
	
	protected GeneratedRealoccaddonConstants()
	{
		// private constructor
	}
	
	
}
