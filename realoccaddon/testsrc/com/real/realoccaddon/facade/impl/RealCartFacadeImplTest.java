package com.real.realoccaddon.facade.impl;

import com.real.realoccaddon.service.RealCartService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
class RealCartFacadeImplTest {

    private UserModel userModel;
    private CartModel cartModel;
    private CartData cartData;
    private CommerceCartParameter parameter;
    private CommerceCartModification modification;
    private CartModificationData cartModificationData;

    @Mock
    private RealCartService cartService;

    @Mock
    private Converter<CartModel, CartData> cartConverter;

    @Mock
    private Converter<AddToCartParams, CommerceCartParameter> commerceCartParameterConverter;

    @Mock
    private CommerceCartService commerceCartService;

    @Mock
    private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

    @InjectMocks
    private RealCartFacadeImpl realCartFacade = new RealCartFacadeImpl();

    @Before
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userModel = new UserModel();
        userModel.setUid("userId");
        cartModel = new CartModel();
        cartData = new CartData();
        cartData.setCode("cartId");
        PrincipalData userData = new PrincipalData();
        userData.setUid("userId");
        cartData.setUser(userData);
        parameter = new CommerceCartParameter();
        modification = new CommerceCartModification();
        cartModificationData = new CartModificationData();
        cartModificationData.setCartCode("cartId");
    }

    @Test
    void getCartForUserAndCode() {
        when(cartService.getCartForUserAndCode("cartId", "userId")).thenReturn(cartModel);

        CartData actualResult = realCartFacade.getCartForUserAndCode("cartId", "userId");

        verify(cartConverter, times(1)).convert(cartModel);
    }

    @Test(expected = ModelNotFoundException.class)
    void throwExceptionIfEntriesAreMissing() throws CommerceCartModificationException {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);
        List<OrderEntryData> entries = new ArrayList<>();
        cartData.setEntries(entries);
        when(cartConverter.convert(cartModel)).thenReturn(cartData);

        realCartFacade.removeProductFromCart("productSku");
    }

    @Test
    void updateEntryIfProductIsFound() throws CommerceCartModificationException {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);
        List<OrderEntryData> entries = new ArrayList<>();
        OrderEntryData orderEntryData = new OrderEntryData();
        ProductData productData = new ProductData();
        productData.setCode("productSku");
        orderEntryData.setProduct(productData);
        orderEntryData.setEntryNumber(0);
        cartData.setEntries(entries);
        when(cartConverter.convert(cartModel)).thenReturn(cartData);
        when(commerceCartParameterConverter.convert(any())).thenReturn(parameter);
        when(commerceCartService.updateQuantityForCartEntry(parameter)).thenReturn(modification);
        when(cartModificationConverter.convert(modification)).thenReturn(cartModificationData);

        CartModificationData actualResult = realCartFacade.removeProductFromCart("productSku");

        verify(actualResult.getCartCode().equals("cartId"));
    }

    @Test(expected = ModelNotFoundException.class)
    void throwExceptionIfProductIsNotFoundInCart() throws CommerceCartModificationException {
        when(cartService.hasSessionCart()).thenReturn(true);
        when(cartService.getSessionCart()).thenReturn(cartModel);
        List<OrderEntryData> entries = new ArrayList<>();
        OrderEntryData orderEntryData = new OrderEntryData();
        orderEntryData.setEntryNumber(0);
        cartData.setEntries(entries);

        realCartFacade.removeProductFromCart("productSku");
    }

    @After
    void tearDown() {
    }
}