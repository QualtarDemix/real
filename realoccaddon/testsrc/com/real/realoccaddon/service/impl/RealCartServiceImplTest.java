package com.real.realoccaddon.service.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RealCartServiceImplTest {

    public static final String SESSION_CART_PARAMETER_NAME = "cart";

    private UserModel userModel;

    @Spy
    private CartModel cartModel;

    @Mock
    private UserService userService;

    @Mock
    private CommerceCartService commerceCartService;

    @Mock
    private SessionService sessionService;

    @Mock
    private ModelService modelService;

    @InjectMocks
    private RealCartServiceImpl realCartService = new RealCartServiceImpl();

    @Before
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userModel = new UserModel();
        userModel.setUid("userId");
        cartModel.setCode("cartId");
        cartModel.setUser(userModel);
    }

    @Test(expected = ModelNotFoundException.class)
    void throwExceptionWhenUserIsNotFound() {
        when(userService.getUserForUID(any(String.class))).thenReturn(null);

        realCartService.getCartForUserAndCode(any(String.class), any(String.class));
    }

    @Test(expected = ModelNotFoundException.class)
    void throwExceptionWhenCartIsNotFound() {
        when(userService.getUserForUID(any(String.class))).thenReturn(any(UserModel.class));
        when(commerceCartService.getCartForCodeAndUser(any(String.class), any(UserModel.class))).thenReturn(null);

        realCartService.getCartForUserAndCode(any(String.class), any(String.class));
    }

    @Test
    void returnCartDataForValidUserAndCart() {
        when(userService.getUserForUID("userId")).thenReturn(userModel);
        when(commerceCartService.getCartForCodeAndUser("cartId", userModel)).thenReturn(cartModel);

        CartModel actualResult = realCartService.getCartForUserAndCode("userId", "cartId");

        verify(actualResult.getCode().equals("cartId"));
        verify(actualResult.getUser().getUid().equals("userId"));
    }

    @Test
    void updateCartName() {
        when(sessionService.getOrLoadAttribute(SESSION_CART_PARAMETER_NAME, any())).thenReturn(cartModel);

        realCartService.updateCartName("cartName");

        verify(modelService, times(1)).save(cartModel);
        verify(cartModel).setName("cartName");
    }

    @After
    void tearDown() {
    }
}