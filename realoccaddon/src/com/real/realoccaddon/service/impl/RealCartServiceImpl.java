package com.real.realoccaddon.service.impl;

import com.real.realoccaddon.service.RealCartService;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.user.UserService;

public class RealCartServiceImpl extends DefaultCartService implements RealCartService {

    private UserService userService;
    private CommerceCartService commerceCartService;

    @Override
    public CartModel getCartForUserAndCode(String cartId, String userId) {
        UserModel userModel = getUserService().getUserForUID(userId);
        if (null == userModel) {
            throw new ModelNotFoundException("User not found for id : " + userId);
        }
        CartModel cartModel = getCommerceCartService().getCartForCodeAndUser(cartId, userModel);
        if (null == cartModel) {
            throw new ModelNotFoundException("Cart not found for user : " + userId + " and cart id : " + cartId);
        }
        return cartModel;
    }

    @Override
    public void updateCartName(String name) {
        CartModel cartModel = getSessionCart();
        cartModel.setName(name);
        getModelService().save(cartModel);
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public CommerceCartService getCommerceCartService() {
        return commerceCartService;
    }

    public void setCommerceCartService(CommerceCartService commerceCartService) {
        this.commerceCartService = commerceCartService;
    }
}
