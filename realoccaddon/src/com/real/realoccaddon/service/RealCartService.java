package com.real.realoccaddon.service;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

public interface RealCartService extends CartService {

    CartModel getCartForUserAndCode(String cartId, String userId);

    void updateCartName(String name);
}
