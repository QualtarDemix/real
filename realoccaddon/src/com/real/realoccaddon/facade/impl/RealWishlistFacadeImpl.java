package com.real.realoccaddon.facade.impl;


import com.real.realoccaddon.facade.RealWishlistFacade;
import com.realoccaddon.wishlist.data.RealAddToWishlistRequestData;
import com.realoccaddon.wishlist.data.WishlistData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;


/**
 * Default implementation of {@link RealWishlistFacade} interface.
 *
 * @author Farrukh Chishti.
 */
public class RealWishlistFacadeImpl implements RealWishlistFacade {
    private static final String WISHLIST_UID_SEPARATOR = "_";
    private static Logger LOG = Logger.getLogger(RealWishlistFacadeImpl.class);

    private UserService userService;
    private ProductService productService;
    private ModelService modelService;
    private Wishlist2Service wishlistService;
    private Converter<Wishlist2Model, WishlistData> wishlistConverter;

    /**
     * Adds the product whose code is given to the default wish list of current customer. If current customer does not
     * have a default wish list, then an empty wish list is created, set as default, the product given is added to it and
     * the updated wish list is returned.
     * <p>
     * quantity to add
     *
     * @return updated wish list.
     */
    @Override
    public WishlistData addToWishList(RealAddToWishlistRequestData requestData) {
        String productCode = requestData.getProductCode();
        Integer desiredQuantity = requestData.getQuantity();
        final ProductModel productModel = productService.getProductForCode(productCode);
        final UserModel userModel = userService.getCurrentUser();

        Wishlist2Model defaultWishlist = getDefaultWishlist(userModel);
        final Wishlist2EntryModel wishlist2EntryModel = wishlistService.getWishlistEntryForProduct(productModel, defaultWishlist);
        if (wishlist2EntryModel == null) {
            wishlistService.addWishlistEntry(userModel, productModel, desiredQuantity, Wishlist2EntryPriority.HIGH, "Must have product");
        }

        return wishlistConverter.convert(defaultWishlist);
    }

    /**
     * Fetches the default wish list for current customer. If current customer does not have a default wish list, then an
     * empty wish list is created, set as default and returned.
     *
     * @return default wish list of current customer.
     */
    @Override
    public WishlistData getWishList() {
        final UserModel userModel = userService.getCurrentUser();
        Wishlist2Model defaultWishlist = getDefaultWishlist(userModel);
        return wishlistConverter.convert(defaultWishlist);
    }

    /**
     * Removes the wish product whose code is given from the default wish list of current user. If current user does not
     * have a default wish list, then an empty wish list is created, set as default and returned.
     *
     * @param productCode code of the product to be removed from the wish list
     */
    @Override
    public void removeFromWishList(final String productCode) {
        final ProductModel productModel = productService.getProductForCode(productCode);
        final UserModel userModel = userService.getCurrentUser();
        Wishlist2Model defaultWishlist = getDefaultWishlist(userModel);
        wishlistService.removeWishlistEntryForProduct(productModel, defaultWishlist);
    }


    /**
     * Creates a new wish list for current customer in current site.
     *
     * @param wishlistName name for which wish list is to be created.
     * @return wish list created.
     */
    @Override
    public WishlistData createWishlist(final String wishlistName, final String wishlistDetails) {
        final UserModel userModel = userService.getCurrentUser();
        List<Wishlist2Model> wishlists = wishlistService.getWishlists(userModel);
        boolean wishlistAlreadyExists = wishlists.stream()
                .anyMatch(wishList -> wishList.getName().equalsIgnoreCase(wishlistName));
        if (!wishlistAlreadyExists) {
            LOG.debug("Creating new wishlist by the name : " + wishlistName);
            Wishlist2Model wishlistModel = wishlistService.createWishlist(userModel, wishlistName, wishlistDetails);
            return wishlistConverter.convert(wishlistModel);
        } else {
            throw new AmbiguousIdentifierException("Wishlist by the name : " + wishlistName + " already exists");
        }
    }

    /**
     * Edits the name of the wishlist
     *
     * @param oldName to identify the wishlist whose name is going to be edited
     * @param newName the new name that will be assigned to the wishlist
     * @return modified wish list
     */
    @Override
    public void updateWishlistName(final String oldName, final String newName) {
        final UserModel userModel = userService.getCurrentUser();
        List<Wishlist2Model> wishlists = wishlistService.getWishlists(userModel);
        boolean newNameWishlistAlreadyExists = wishlists.stream()
                .anyMatch(wishList -> wishList.getName().equalsIgnoreCase(newName));
        if (newNameWishlistAlreadyExists) {
            throw new ModelNotFoundException("Wishlist with name : " + newName + " already exists");
        }
        Wishlist2Model wishlistModel = wishlists.stream()
                .filter(wishlist -> wishlist.getName().equalsIgnoreCase(oldName))
                .findFirst()
                .orElseThrow(() -> new ModelNotFoundException("Wishlist with name : " + oldName + " not found"));
        wishlistModel.setName(newName);
        modelService.save(wishlistModel);
    }

    private Wishlist2Model getDefaultWishlist(UserModel userModel) {
        if (!wishlistService.hasDefaultWishlist(userModel)) {
            String wishlistName = createWishlistName(userModel);
            LOG.debug("Wishlist missing for : " + wishlistName + ". Creating new wishlist by the name : " + wishlistName);
            return wishlistService.createDefaultWishlist(userModel, wishlistName, "This is default wishlist");
        } else {
            return wishlistService.getDefaultWishlist(userModel);
        }
    }

    private String createWishlistName(UserModel userModel) {
        return new StringBuilder()
                .append(userModel.getUid())
                .append(WISHLIST_UID_SEPARATOR)
                .append(new Date().getTime()).toString();
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public ModelService getModelService() {
        return modelService;
    }

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public Wishlist2Service getWishlistService() {
        return wishlistService;
    }

    public void setWishlistService(Wishlist2Service wishlistService) {
        this.wishlistService = wishlistService;
    }

    public Converter<Wishlist2Model, WishlistData> getWishlistConverter() {
        return wishlistConverter;
    }

    public void setWishlistConverter(Converter<Wishlist2Model, WishlistData> wishlistConverter) {
        this.wishlistConverter = wishlistConverter;
    }
}
