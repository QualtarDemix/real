package com.real.realoccaddon.facade.impl;

import com.real.realoccaddon.facade.RealCartFacade;
import com.real.realoccaddon.service.RealCartService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.List;
import java.util.Optional;

public class RealCartFacadeImpl extends DefaultCartFacade implements RealCartFacade {

    private RealCartService cartService;

    @Override
    public CartData getCartForUserAndCode(String userId, String cartId) {
        CartModel cartModel = cartService.getCartForUserAndCode(userId, cartId);
        return getCartConverter().convert(cartModel);
    }

    @Override
    public CartModificationData removeProductFromCart(String productSku) throws CommerceCartModificationException {
        long entryNumber = getEntryNumberForProduct(productSku);
        return updateCartEntry(entryNumber, 0);
    }

    @Override
    public void updateCartName(String name) {
        cartService.updateCartName(name);
    }

    private long getEntryNumberForProduct(String productSku) {
        CartData cartData = getSessionCart();
        List<OrderEntryData> entries = cartData.getEntries();
        OrderEntryData orderEntryData = Optional.ofNullable(entries)
                .filter(entry -> !entry.isEmpty())
                .orElseThrow(() -> new ModelNotFoundException("Order entries not found for cart :" + cartData.getCode()))
                .stream()
                .filter(entry -> entry.getProduct().getCode().equals(productSku))
                .findFirst()
                .orElseThrow(() -> new ModelNotFoundException("Product missing in cart : " + cartData.getCode()));
        return orderEntryData.getEntryNumber().longValue();
    }

    public RealCartService getCartService() {
        return cartService;
    }

    public void setCartService(RealCartService cartService) {
        this.cartService = cartService;
    }
}
