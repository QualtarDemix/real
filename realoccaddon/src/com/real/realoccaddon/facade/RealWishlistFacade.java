package com.real.realoccaddon.facade;

import com.realoccaddon.wishlist.data.RealAddToWishlistRequestData;
import com.realoccaddon.wishlist.data.WishlistData;


/**
 * Facade interface for wish list related functionality.
 *
 * @author Farrukh Chishti
 */
public interface RealWishlistFacade
{

	/**
	 * Adds the product whose code is given to the default wish list of current customer. If current customer does not
	 * have a default wish list, then an empty wish list is created, set as default, the product given is added to it and
	 * the updated wish list is returned.
	 *
	 *           quantity to add
	 * @return updated wish list.
	 */
	WishlistData addToWishList(RealAddToWishlistRequestData requestData);

	/**
	 * Fetches the default wish list for current customer. If current customer does not have a default wish list, then an
	 * empty wish list is created, set as default and returned.
	 *
	 * @return default wish list of current customer.
	 */
	WishlistData getWishList();

	/**
	 * Removes the wish product whose code is given from the default wish list of current user. If current user does not
	 * have a default wish list, then an empty wish list is created, set as default and returned.
	 *
	 * @param productCode
	 *           code of the product to be removed from the wish list
	 */
	void removeFromWishList(final String productCode);

	/**
	 * Creates a new wish list for current customer in current site.
	 *
	 * @param wishlistName
	 *           name for which wish list is to be created.
	 * @return wish list created.
	 */
	WishlistData createWishlist(final String wishlistName, final String wishlistDetails);

	/**
	 * Edits the name of the wishlist
	 *
	 * @param oldName
	 *           to identify the wishlist whose name is going to be edited
	 * @param newName
	 *           the new name that will be assigned to the wishlist
	 * @return modified wish list
	 */
	void updateWishlistName(String oldName, String newName);
}
