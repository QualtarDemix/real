package com.real.realoccaddon.facade;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

public interface RealCartFacade {

    CartData getCartForUserAndCode(String userId, String cartId);

    CartModificationData removeProductFromCart(String productSku) throws CommerceCartModificationException;

    void updateCartName(String name);
}
