package com.real.realoccaddon.populators;


import com.realoccaddon.wishlist.data.WishlistData;
import com.realoccaddon.wishlist.data.WishlistEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;
import org.apache.commons.lang.BooleanUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author Farrukh Chishti
 */
public class RealWishlistPopulator implements Populator<Wishlist2Model, WishlistData> {
    private Converter<ProductModel, ProductData> productConverter;

    @Override
    public void populate(final Wishlist2Model source, final WishlistData target) throws ConversionException {
        target.setDescription(source.getDescription());
        target.setIsDefault(BooleanUtils.toStringTrueFalse(source.getDefault()));
        target.setName(source.getName());
        List<Wishlist2EntryModel> entries = source.getEntries();
        List<WishlistEntryData> targetEntires = Optional.ofNullable(entries)
                .filter(entry -> !entry.isEmpty())
                .orElse(Collections.emptyList())
                .stream()
                .map(sourceEntry -> getWishlistEntryData(sourceEntry))
                .collect(Collectors.toList());
        target.setEntries(targetEntires);
    }

    private WishlistEntryData getWishlistEntryData(Wishlist2EntryModel sourceEntry) {
        final WishlistEntryData entry = new WishlistEntryData();
        entry.setProduct(getProductConverter().convert(sourceEntry.getProduct()));
        entry.setDesired(sourceEntry.getDesired());
        entry.setReceived(sourceEntry.getReceived());
        entry.setPriority(sourceEntry.getPriority().getCode());
        entry.setAddedDate(sourceEntry.getAddedDate());
        entry.setComment(sourceEntry.getComment());
        return entry;
    }

    public Converter<ProductModel, ProductData> getProductConverter() {
        return productConverter;
    }

    public void setProductConverter(final Converter<ProductModel, ProductData> productConverter) {
        this.productConverter = productConverter;
    }
}
