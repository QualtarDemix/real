package com.real.realoccaddon.controllers;

import com.myproject.core.v2.controller.BaseCommerceController;
import com.real.realoccaddon.facade.RealWishlistFacade;
import com.realoccaddon.wishlist.WishlistWsDTO;
import com.realoccaddon.wishlist.data.RealAddToWishlistRequestData;
import com.realoccaddon.wishlist.data.WishlistData;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/wishlist")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Wishlist")
public class RealWishlistController extends BaseCommerceController {

    @Resource(name = "realWishlistFacade")
    private RealWishlistFacade realWishlistFacade;

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(path = "/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Adds product to wishlist", notes = "Adds product to wishlist")
    public WishlistWsDTO addToWishlist(@ApiParam(value = "Payload for adding product to wishlist", required = true)
                                       @RequestBody RealAddToWishlistRequestData payload,
                                       @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                       @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        WishlistData wishlistData = realWishlistFacade.addToWishList(payload);
        return getDataMapper().map(wishlistData, WishlistWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get wishlist details", notes = "Get wishlist details")
    public WishlistWsDTO getWishlist(@ApiParam(value = "User id") @PathVariable final String userId,
                                     @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                     @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        WishlistData wishlistData = realWishlistFacade.getWishList();
        return getDataMapper().map(wishlistData, WishlistWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Remove product from wishlist")
    public void removeProductFromWishlist(@ApiParam(value = "Product sku", required = true) @RequestParam(required = true) final String productSku) {
        realWishlistFacade.removeFromWishList(productSku);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create wishlist")
    public WishlistWsDTO createWishlist(@ApiParam(value = "Wishlist name") @RequestParam(required = true) final String name,
                                        @ApiParam(value = "Wishlist details") @RequestParam(required = false) final String details,
                                        @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                        @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        WishlistData wishlistData = realWishlistFacade.createWishlist(name, details);
        return getDataMapper().map(wishlistData, WishlistWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update wishlist name")
    public void updateWishlistName(@ApiParam(value = "Old wishlist name") @RequestParam(required = true) final String oldName,
                                   @ApiParam(value = "New wishlist name") @RequestParam(required = true) final String newName,
                                   @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                   @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        realWishlistFacade.updateWishlistName(oldName, newName);
    }
}
