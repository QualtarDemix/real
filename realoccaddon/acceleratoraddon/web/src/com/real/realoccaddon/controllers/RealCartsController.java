package com.real.realoccaddon.controllers;

import com.myproject.core.swagger.ApiBaseSiteIdUserIdAndCartIdParam;
import com.myproject.core.v2.controller.BaseCommerceController;
import com.real.realoccaddon.facade.RealCartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.request.mapping.annotation.RequestMappingOverride;
import de.hybris.platform.commercewebservicescommons.dto.order.CartModificationWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.order.CartWsDTO;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.ProductLowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.StockSystemException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
@Api(tags = "Carts")
public class RealCartsController extends BaseCommerceController {

    private static final Logger LOG = Logger.getLogger(RealCartsController.class);

    @Resource(name = "cartFacade")
    private RealCartFacade cartFacade;

    @Resource(name = "realStockValidator")
    private Validator realStockValidator;

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMappingOverride
    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "Get cart details for cart-id.")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public CartWsDTO getCart(@ApiParam(value = "User id") @PathVariable final String userId,
                             @ApiParam(value = "Cart id") @PathVariable final String cartId,
                             @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                             @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields) {
        CartData cartData = cartFacade.getCartForUserAndCode(userId, cartId);
        return getDataMapper().map(cartData, CartWsDTO.class, fields);
        // Alternatively, CartMatchingFilter sets current cart based on cartId, so we can return cart from the session
        //return getDataMapper().map(getSessionCart(), CartWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMappingOverride
    @RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiOperation(value = "Adds a product to the cart.", notes = "Adds a product to the cart.")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public CartModificationWsDTO addCartEntry(@ApiParam(value = "Product sku", required = true) @RequestParam(required = true) final String productSku,
                                              @ApiParam(value = "Quantity", required = true) @RequestParam(required = true, defaultValue = "1") final Long quantity,
                                              @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                              @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields)
            throws CommerceCartModificationException, WebserviceValidationException, ProductLowStockException, StockSystemException {
        //Check stock before adding product to the cart
        validate(productSku, "String", realStockValidator);
        //Cart id is already set in CartMatchingFilter
        final CartModificationData cartModificationData = getCartFacade().addToCart(productSku, quantity);
        return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(value = "/{cartId}/entries", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Remove product from cart")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public CartModificationWsDTO removeCartEntry(@ApiParam(value = "Product sku", required = true) @RequestParam(required = true) final String productSku,
                                                 @ApiParam(value = "List of fields to be returned", allowableValues = "BASIC, DEFAULT, FULL")
                                                 @RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fields)
            throws CommerceCartModificationException {
        CartModificationData cartModificationData = cartFacade.removeProductFromCart(productSku);
        return getDataMapper().map(cartModificationData, CartModificationWsDTO.class, fields);
    }

    @Secured("ROLE_REAL_DIGITAL_GROUP")
    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update cart name")
    @ApiBaseSiteIdUserIdAndCartIdParam
    public void updateCartName(@ApiParam(value = "Cart name") @RequestParam(required = true) final String name) {
        cartFacade.updateCartName(name);
    }

}
