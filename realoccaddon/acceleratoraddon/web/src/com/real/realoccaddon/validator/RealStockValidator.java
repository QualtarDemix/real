package com.real.realoccaddon.validator;

import com.myproject.core.stock.CommerceStockFacade;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.LowStockException;
import de.hybris.platform.commercewebservicescommons.errors.exceptions.ProductLowStockException;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class RealStockValidator implements Validator {

    private CommerceStockFacade commerceStockFacade;
    private BaseSiteService baseSiteService;

    @Override
    public boolean supports(Class<?> clazz) {
        return String.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        String productSku = (String) target;
        String baseSiteId = baseSiteService.getCurrentBaseSite().getUid();
        final StockData stock = commerceStockFacade.getStockDataForProductAndBaseSite(productSku, baseSiteId);
        if (stock == null) {
            errors.reject("Stock details not available for product : " + productSku);
        }
        if (stock.getStockLevelStatus().equals(StockLevelStatus.OUTOFSTOCK)) {
            throw new ProductLowStockException("Product [" + productSku + "] cannot be shipped - out of stock",
                    LowStockException.NO_STOCK, productSku);
        }
    }

    public CommerceStockFacade getCommerceStockFacade() {
        return commerceStockFacade;
    }

    public void setCommerceStockFacade(CommerceStockFacade commerceStockFacade) {
        this.commerceStockFacade = commerceStockFacade;
    }

    public BaseSiteService getBaseSiteService() {
        return baseSiteService;
    }

    public void setBaseSiteService(BaseSiteService baseSiteService) {
        this.baseSiteService = baseSiteService;
    }
}

