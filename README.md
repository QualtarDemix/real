# Real Digital

Real digital interview API's
Index-

1. Setup details
2. Wishlist
    1. Implementation details
    2. Technical details
3. Cart
    1. Implementation details
    2. Technical details
---

## Setup details:

* Hybris version- 6.6.0.4
* Java - 1.8
* IDE - IntelliJ

I have created a new OCC add-on (by the name **realoccaddon**) and added all my code in this add-on.

For creating the add-on I have used the command-
> `ant extgen -Dinput.template= ycommercewebservices -Dinput.name=realoccaddon -Dinput.package=com.real.realoccaddon`

For installing this add-on in my hybris project, I have used the command-
> `ant addoninstall -Daddonnames="realoccaddon" -DaddonStorefront.ycommercewebservices=“myprojectcommercewebservices"`

You can also install this add-on using the above command. Replace, ‘*myprojectcommercewebservices*’ with your base ‘*commercewebservices*’ module.

After running the build and server you can hit the API’s using below postman collection.
API’s link : https://www.getpostman.com/collections/f7af94317292e9628096

---

# Wishlist

## Implementation details:

### Develop API where each product can be added to a wishlist – 
+ OCC call to add product to wishlist.
+ URL- `/{baseSiteId}/users/{userId}/wishlist/add`
+ Type : ***POST***
+ Body params : RealAddToWishlistRequestData
    * productCode : Product sku
    * quantity : Desired quantity
+ Response - WishlistWsDTO

### Develop API where wishlist can be viewed – 
+ OCC call to view wishlist
+ URL- `/{baseSiteId}/users/{userId}/wishlist`
+ Type : ***GET***
+ Response – WishlistWsDTO

### Develop API where Products can be removed from wishlist – 
+ OCC call to remove products from cart.
+ URL- `/{baseSiteId}/users/{userId}/wishlist/remove`
+ Type : ***DELETE***
+ Params : product sku
+ Response – 200 OK.

### Develop API where name of wishlist can be defined-
+ URL- `/{baseSiteId}/users/{userId}/wishlist/create`
+ Type : ***PUT***
+ Params : 
    * name : Name of wishlist
    * details : Details of wishlist
+ Response – WishlistWsDTO

### Develop API where name of wishlist can be changed-
+ URL- `/{baseSiteId}/users/{userId}/wishlist/create`
+ Type : ***PUT***
+ Params : 
    * oldName : Old name of wishlist
    * newName : New name of wishlist
+ Response – 200 OK.

Exceptions will be thrown in all the API’s if the data i.e. name, userId or sku’s are invalid.

### Shopping list is only available for logged in customers – 
Secured individual API call with `@Secured(ROLE_REAL_DIGITAL_GROUP)`. Created an `OAuthClientDetails` with access only to `ROLE_REAL_DIGITAL_GROUP`.

---

# Cart

## Implementation details:

### Develop API where each product can be added to a shopping list – 
+ OCC call to add product to cart.
+ URL- `/{baseSiteId}/users/{userId}/carts/{cartId}/entries`
+ Type : ***POST***
+ Body params : sku of product, quantity (default = 1)
+ Response - CartModificationWsDTO

### Develop API where Shopping list can be viewed – 
+ OCC call to view cart
+ URL- `/{baseSiteId}/users/{userId}/carts/{cartId}`
+ Type : ***GET***
+ Response – CartWsDTO

### Develop API where Products can be removed from shopping list – 
+ OCC call to remove products from cart.
+ URL- `/{baseSiteId}/users/{userId}/carts/{cartId}/entries`
+ Type : ***DELETE***
+ Params : product sku
+ Response – CartModificationWsDTO.

### Develop API where Name of shopping lists can be defined and changed – 
+ URL- `/{baseSiteId}/users/{userId}/carts/{cartId}/name`
+ Type : ***PUT***
+ Params : name of cart
+ Response – None. Just 200 OK status.

Exceptions will be thrown in all the API’s if the data i.e. cartId, userId or sku’s are invalid.

### Shopping list is only available for logged in customers – 
Secured individual API call with `@Secured(ROLE_REAL_DIGITAL_GROUP)`. Created an `OAuthClientDetails` with access only to `ROLE_REAL_DIGITAL_GROUP`.

---

## Technical details:

### Presentation layer- Exposed API’s, applied authorizations and created validators.

1.	RealCartsController.java  - Common controller for all the API’s as all of them are related to carts. All the methods are secured with `@Secured("ROLE_REAL_DIGITAL_GROUP")`, so only the OCC clients with access to `ROLE_REAL_DIGITAL_GROUP` can hit these API’s. Mapped with `/{baseSiteId}/users/{userId}/carts`. Complete path: `com.real.realoccaddon.controllers.RealCartsController`. Methods-
  
  *	getCart() – 
  
      + Mapped with `/{cartId}` 
      + Type : ***GET***
      + Gets cart details.
      + Returns - CartWsDTO

  * addCartEntry() – 
  
      + Mapped with `/{cartId}/entries`
      + Type : ***POST***
      + Parameters: productSku and quantity (default = 1)
      + Adds product to cart. Also, validates product for stock-check before adding it to the cart.
      + Returns - CartModifcationWsDTO

  *	removeCartEntry() – 
  
      + Mapped with `/{cartId}/entries` 
      + Type : ***DELETE***
      + Parameter: Product sku
      + Removes the cart entry for product.
      + Returns - CartModificationWsDTO

  *	updateCartName() – 
  
      + Mapped with `/{cartId}`
      + Type : ***PUT***
      + Parameter : Product sku
      + Updates the name of cart.
      + Does not return anything. If the process was successful it simply responds with 200 OK status.


2.	RealStockValidator.java – Validator to check if product is in stock. Complete path: `com.real.realoccaddon.validator.RealStockValidator`. Methods-
  
  *	validate() – 
  
    + Checks stock of product.
    + Response-
    
      1.	Shows error message if the stock details for the product are missing.
      2.	Throws exception if the products is out of stock.

### Façade layer – Call the services to update the cart attributes. Convert the models to data and forward to controller.

1.	RealCartFacade.java – Interface for updating cart attributes.
Complete path : com.real.realoccaddon.facade.RealCartFacade
2.	RealCartFacadeImpl.java – Implements RealCartFacade.java interface and forwards data to controller. 
Complete path : com.real.realoccaddon.facade.impl.RealCartFacadeImpl
Methods-
i.	getCartForUserAndCode() – Gets cart for user id and cart id.
Parameters- user id and cart id
Throws exception if user id or cart id are invalid.
Returns cart data.
ii.	removeProductFromCart() – Gets entry number of entry containing the product. Then updates that entry with quantity = 0, thereby removing it.
Parameters- product sku
Throws exception if the cart is missing entries or the product.
Returns cart modification data.
iii.	updateCartName() -  Updates cart name
Parameters : Cart name
3.	RealCartFacadeImplTest.java – Contains the junit tests for RealCartFacadeImpl.java.
Complete path : com.real.realoccaddon.facade.impl.RealCartFacadeImplTest
Methods-
a.	getCartForUserAndCode()
b.	throwExceptionIfEntriesAreMissing()
c.	updateEntryIfProductIsFound()
d.	throwExceptionIfProductIsNotFoundInCart()


### Service layer – Business layer to execute custom logic.

1.	RealCartService.java – Interface to update cart attributes.
Complete path : com.real.realoccaddon.service.RealCartService
2.	RealCartServiceImpl.java – Implements RealCartService.java and applies business logic to modify cart attributes. 
Complete path : com.real.realoccaddon.service.impl.RealCartServiceImpl
Methods-
a.	getCartForUserAndCode() – Gets cart for user id and cart id.
Parameters- user id and cart id
Throws exception if user id or cart id are invalid.
Returns cart model.
b.	updateCartName() -  Updates cart name
Parameters : Cart name
3.	RealCartServiceImplTest.java – Test class for RealCartServiceImpl.java
Methods-
a.	throwExceptionWhenUserIsNotFound()
b.	throwExceptionWhenCartIsNotFound()
c.	returnCartDataForValidUserAndCart()
d.	updateCartName()

### Data layer – Sample OCC client created

1. essentialdata_realoccaddon.impex – Made entry for new OCC client with access to ROLE_REAL_DIGITAL_GROUP. 
Complete path : realoccaddon/import/essentialdata_realoccaddon.impex
OCC client id : real-employee
Password : secret